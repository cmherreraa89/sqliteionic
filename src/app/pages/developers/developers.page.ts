import { DatabaseService, Dev } from './../../services/database.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { Network } from '@ionic-native/network/ngx';
import { LoginService } from './../../services/login.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.page.html',
  styleUrls: ['./developers.page.scss'],
})
export class DevelopersPage implements OnInit {

  developers: Dev[] = [];

  products: Observable<any[]>;

  developer = {};
  product = {};

  selectedView = 'devs';
  subscriptionDisconnectNetwork: any;
  subscriptionChangeNetwork: any;

  constructor(
    private db: DatabaseService,
    private backgroundMode: BackgroundMode,
    private network: Network,
    platform: Platform,
    private loginSr: LoginService) {
      // this.backgroundMode.enable();
     }

  ngOnInit() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getDevs().subscribe(devs => {
          this.developers = devs;
        });
        this.products = this.db.getProducts();
      }
    });
  }

  async addDeveloper() {
    // enviamos los datos al servidor
    this.loginSr.login( '80097672', 'dieglamm').then(
      relsult => {
        // console.log(relsult);
        // Si no hay conexión a internet guardamos en local
        if (relsult === false)  {
          this.backgroundMode.enable();
          this.conection();
        }
       }
    );
  }

  sqlliteForm() {
    // console.log('sqlliteForm');
    // tslint:disable-next-line:no-string-literal
    let skills = this.developer['skills'].split(',');
    skills = skills.map(skill => skill.trim());

    // tslint:disable-next-line:no-string-literal
    this.db.addDeveloper(this.developer['name'], skills, this.developer['img'])
    .then(_ => {
      this.developer = {};
    });
  }

  // tslint:disable-next-line:one-line
  backgroudMode(){
    // console.log('backgroudMode');
    // mantener activa la aplicacion en segundo plano
    this.backgroundMode.enable();
    this.backgroundMode.on('activate').subscribe(() => {
      return this.saludo();
    });
  }

  conection() {
        // watch network for a disconnection
        let exit;
        // tslint:disable-next-line:prefer-const
        let reConect =  setInterval(() => {
          this.saludo(); // Now the "this" still references the component
          if (this.backgroundMode.on('activate')) {
            exit = this.backgroudMode();
          } else {
            exit = this.saludo();
          }
          // console.log(exit);
          if (exit)  {
            clearInterval(reConect);
          }
      }, 5000);
  }

  saludo() {
    let resquestSend  = false;
    // watch network for a connection
    // tslint:disable-next-line:prefer-const
    let connectSubscription = this.network.onConnect().subscribe(() => {
      // console.log('network connected!');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        // console.log(this.network, 'this.network');
        if (this.network.type !== 'none') {
          // console.log('cuando vuelve internet hacemos la petición');
          this.sqlliteForm();
          connectSubscription.unsubscribe();
          resquestSend = true;
          this.backgroundMode.disable();
        }
      }, 3000);
    },
    );

    return resquestSend;

    // stop connect watch
    // connectSubscription.unsubscribe();
  }

  addProduct() {
    // tslint:disable-next-line:no-string-literal
    this.db.addProduct(this.product['name'], this.product['creator'])
    .then(_ => {
      this.product = {};
    });
  }

  private newMethod(): TimerHandler {
    return 'hola';
  }
}

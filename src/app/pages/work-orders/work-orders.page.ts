import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';

import { TravelService } from './../../services/travel.service';
import { UpdateSuccess } from '../../config/url.service';
import { UpdateError } from '../../config/url.service';

import { FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-work-orders',
  templateUrl: './work-orders.page.html',
  styleUrls: ['./work-orders.page.scss'],
})
export class WorkOrdersPage implements OnInit {

  selectedView = 'registar';
  name:string;
  lastname:string;
  id:string;
  imagen:string = "";

  dataUser: any['identification_type']= "";

  datos={
    'fecha': "",
    'placa': '',
    'operador':'',
    'hora_entrada': '',
    'hora_salida':'',
    'hora_inicial':'',
    'hora_final':'',
    'tiempo_descuento':'',
    'total_horas':'',
    'diesel_surtido':'',
    'costo_diesel':'',
    'material':'',
    'frente_obra':'',
    'fuente':'',
    'destino':'',
    'observaciones':'',
    'aceptado_por':'',
    'factura' : '',
    'cheque': '',
    'valor': '',
    'estado': 'no sincronizado',
  }

  myForm: FormGroup;


  constructor(
    
    private db: TravelService,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) { }

  ngOnInit() {
  }

  saveForm(form){
      console.log(form);
      if ( form.invalid ) { return; }
      this.db.InsertRemote( form.value, 'this.usuario.token').then(data=>{

      });
  
  }

  async mensajes(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}

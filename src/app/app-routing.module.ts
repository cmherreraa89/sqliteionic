import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'developers', loadChildren: './pages/developers/developers.module#DevelopersPageModule' },
  { path: 'developers/:id', loadChildren: './pages/developer/developer.module#DeveloperPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'menu', loadChildren: './pages/menu/menu.module#MenuPageModule' },
  { path: 'report-hours', loadChildren: './pages/report-hours/report-hours.module#ReportHoursPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'reporte-acarreo', loadChildren: './pages/reporte-acarreo/reporte-acarreo.module#ReporteAcarreoPageModule' },
  { path: 'work-orders', loadChildren: './pages/work-orders/work-orders.module#WorkOrdersPageModule' },
  { path: 'report-travel', loadChildren: './pages/report-travel/report-travel.module#ReportTravelPageModule' },
  { path: 'novelty/:hour', loadChildren: './pages/novelty/novelty.module#NoveltyPageModule' },


];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardUserPage } from './dashboard-user.page';

describe('DashboardUserPage', () => {
  let component: DashboardUserPage;
  let fixture: ComponentFixture<DashboardUserPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardUserPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardUserPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

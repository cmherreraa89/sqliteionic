import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-novelty',
  templateUrl: './novelty.page.html',
  styleUrls: ['./novelty.page.scss'],
})
export class NoveltyPage implements OnInit {

  constructor(private route: ActivatedRoute ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      // tslint:disable-next-line:prefer-const
      let hourNovelty = params.get('hour');
      console.log(hourNovelty)
    });
  }
}

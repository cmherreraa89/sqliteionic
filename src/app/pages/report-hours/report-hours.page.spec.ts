import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportHoursPage } from './report-hours.page';

describe('ReportHoursPage', () => {
  let component: ReportHoursPage;
  let fixture: ComponentFixture<ReportHoursPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportHoursPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportHoursPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

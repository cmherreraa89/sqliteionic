import { Component, OnInit } from '@angular/core';
import { DataUserService } from './../../services/data-user.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { LoadingController, ToastController, AlertController } from '@ionic/angular';

import { UpdateSuccess } from '../../config/url.service';
import { UpdateError } from '../../config/url.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  selectedView = 'basic';
  usuario: any;
  confirmarCambio = false;
  // tslint:disable-next-line:variable-name
  photo_new = '../../../assets/imgs/imgProfile.png';
  imagenPreview = '';
  imagen64: string;
  isphone = false;
  imagen = '';
  fotos: any[] = [];
  constructor(
    private db: DataUserService,
    private camera: Camera,
    private transfer: FileTransfer,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController) {
    this.getDataUser();
  }

  ngOnInit() {
  }

  getDataUser() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getUrs().subscribe(usr => {
          this.usuario = usr[0];
          // tslint:disable-next-line:prefer-for-of
          if (this.usuario.fotografia) {
            this.photo_new = 'https://www.grupoc2sas.com/sistema/dist/img/usuarios/' + this.usuario.fotografia;
          }
        });
      }
    });
  }

  tomarFoto() {

    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.photo_new = 'data:image/jpeg;base64,' + imageData;
      this.confirmarCambio = true;
      this.uploadImageServer();
    }, (err) => {
      // Handle error
    });

  }


  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.photo_new = 'data:image/jpeg;base64,' + imageData;
      this.confirmarCambio = true;
      this.uploadImageServer();
    }, (err) => {
      // Handle error
    });
  }

  cropImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 300,
      targetHeight: 300
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.photo_new = 'data:image/jpeg;base64,' + imageData;
      this.confirmarCambio = true;
      this.uploadImageServer();
    }, (err) => {
      // Handle error
    });
  }

  async uploadImageServer() {

    this.confirmarCambio = false;
    // Show loading
    const loader = await this.loadingCtrl.create({
      message: 'Please wait...',
      spinner: 'crescent',
      duration: 2000
    });

    await loader.present();

    // create file transfer object
    const fileTransfer: FileTransferObject = this.transfer.create();

    const date = new Date().getTime();
    const nameImage  = 'GRC2_PERFIL_' + date + '.jpg';

    // option transfer
    const options: FileUploadOptions = {
      fileKey: 'photo',
      fileName: nameImage,
      chunkedMode: false,
      httpMethod: 'post',
      mimeType: 'image/jpeg',
      headers: {
        Connection: 'close'
      }
    };
    // console.log(this.photo_new, options);

    // file transfer action
    fileTransfer.upload(this.photo_new, 'https://www.ardeq.com/api-ardeq/uploadPhoto.php', options)
      .then((data) => {
        // console.log('data-perfil', data);
        this.mensajes('Foto de perfil actualizada');
        loader.dismiss();

        const datos = {
          photo :  nameImage,
          email :  this.usuario.email,
          foto_anterior: this.usuario.photo
        };

      }, (err) => {
        // console.log(err);
        // alert("Error");
        loader.dismiss();
      });
  }

  async mensajes(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

  async editUser(form) {
    console.log(form);
    if ( form.invalid ) { return; }
    const valido = await this.db.editProfile( form.value, this.usuario.token);

    if ( valido ) {
      // navegar al tabs
      this.mensajes(UpdateSuccess);
    } else {
      // mostrar alerta de usuario y contraseña no correctos
      this.mensajes(UpdateError);
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Cambiar foto de perfil!',
      message: 'Selecciona el origen de la imagen',
      buttons: [
        {
          text: 'Cámara',
          handler: () => {
            //console.log('Camara');
            this.tomarFoto();
          }
        }, {
          text: 'Galería',
          handler: () => {
            // console.log('Confirm Okay');
            this.cropImage();
          }
        }
      ]
    });

    await alert.present();
  }

  async changePass() {
    const alert = await this.alertCtrl.create({
      header: 'Cambiar contraseña',
      message: 'Ingresa tu nueva contraseña',
      inputs: [
        {
          label: 'Contraseña',
          name: 'nombres',
          type: 'text',
          placeholder: 'Contraseña',
        },
        {
          label: 'Confirmar contraseña',
          name: 'Hora_entrada',
          type: 'text',
          placeholder: 'Confirmar contraseña',
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (e) => {
            this.UpdatePass(e);
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  UpdatePass(e) {

  }


}

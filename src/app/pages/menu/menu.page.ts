import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { DataUserService, User } from './../../services/data-user.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  user: User[] = [];
  selectedPath = '';
  usuario: any;
  // tslint:disable-next-line:variable-name
  photo_new = '';

  pages = [
    {
      title: 'Ordenes de trabajo',
      url: '/menu/work-orders'
    },
    {
      title: 'Control de horario',
      url: '/menu/report-hours'
    }, 
    {
      title: 'Editar perfil',
      url: '/menu/profile'
    }
  ];

  constructor(
    private router: Router,
    private db: DataUserService,
    private platform: Platform) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
    if (this.platform.is('cordova')) {
      this.getDataUser();
    }
  }

  getDataUser() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getUrs().subscribe(usr => {
          this.usuario = usr[0];
          // tslint:disable-next-line:prefer-for-of
          if (this.usuario.fotografia) {
            this.photo_new = 'https://www.grupoc2sas.com/sistema/dist/img/usuarios/' + this.usuario.fotografia;
          }
        });
      }
    });
  }

  ngOnInit() {
  }

  editProfile() {
    this.router.navigateByUrl('/menu/profile');
  }

  loguot() {
    // console.log('cerrar sesión');
    this.db.deleteUsuario('1').then(() => {
      this.router.navigateByUrl('/login');
    });
  }

}


import { Component, OnInit } from '@angular/core';
import { DataUserService } from './../../services/data-user.service';
import { NavController, ToastController, Platform } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  // tslint:disable-next-line:no-shadowed-variable
  constructor(
    private navCtrl: NavController,
    public toastCtrl: ToastController,
    private db: DataUserService,
    private platform: Platform,
    private loginSr: DataUserService) {
      if (this.platform.is('cordova')) {
        this.getDataUser();
      }
   }

  getDataUser() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.db.getUrs().subscribe(usr => {
          if (usr[0].fotografia) {
            this.navCtrl.navigateRoot( 'menu/report-hoursgit ', { animated: true } );
          }
        });
      }
    });
  }

  ngOnInit() {
  }

  async login( form ) {

    if ( form.invalid ) { return; }

    const valido = await this.loginSr.login( form.value.identification, form.value.password );

    if ( valido ) {
      // navegar al tabs
      // console.log(valido);
      this.navCtrl.navigateRoot( '/menu/report-hours', { animated: true } );
      this.toast('Bienvenido.');
    } else {
      // mostrar alerta de usuario y contraseña no correctos
      // console.log('Usuario y contraseña no son correctos.');
      this.toast('Usuario y contraseña no son correctos.');
    }

  }

  async toast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }

}


import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { HoursService, Hours } from './../../services/hours.service';
import { DataUserService } from './../../services/data-user.service';
import { EmployeesService } from './../../services/employees.service';
import { Platform, NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-report-hours',
  templateUrl: './report-hours.page.html',
  styleUrls: ['./report-hours.page.scss'],
})
export class ReportHoursPage implements OnInit {
  selectedView = 'consultar';
  datos = {
    dateSelect: ''
  };
  newArray: any = [];
  newArrayReport: any = [];
  arraypaginador : any = [];
  arraypaginadorRegister : any = [];
  arrayPagMostar : any = [];
  arrayPagMostarRegister : any = [];
  arraySelect = [];
  arraySelectReport = [];
  prevPag = 0;
  nextPag = 5;
  prevPagRegister = 0;
  nextPagRegister = 5;
  lentPag = 0;
  usuario: any;
  hours: any = [];
  hoursRegister: any = [];
  token = '';
  searchTerm = '';
  searching: any = false;
  selectDate = '';
  dayStorage = 15;
  RegisterHorario :  any = [];
  FrenteDeObra : any = [];

  constructor(
    public alertCtrl: AlertController,
    private hoursSrv: HoursService,
    private db: DataUserService,
    private bdEmployees: EmployeesService,
    private navCtrl: NavController,
    public toastCtrl: ToastController,
    private platform: Platform
  ) {
    // console.log(this.datos.dateSelect);
    this.selectDate = this.ngmodelSelectDate();
    // console.log(this.selectDate);
    if (this.platform.is('cordova')) {
      // this.getDataUser(); 
      this.getDataUser(); 
    }
    
  }

  ngmodelSelectDate() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let day = '';
    let month = '';
    let yyyy = today.getFullYear();
    if (dd < 10) {
      day = '0' + dd;
    } 
    if (mm < 10) {
      month = '0' + mm;
    } 
    return (day + '-' + month + '-' + yyyy);
  }

  getEmployees() {
    this.bdEmployees.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.bdEmployees.loadAllUsuarios().then(usr => {
          // console.log(usr);
          let arrayAddHours: any = usr;
          arrayAddHours.forEach(function callback(posicion, index, array)  {
           // console.log(posicion, index, array);
            posicion['hora_entrada']= '8:00 am';
            posicion['hora_salida']= '7:00 pm';
            posicion['alimentacion']= 1;
            posicion['transporte']= 2;
            posicion['observacion']= 'Ninguna';
            posicion['novedades']= 'Ninguna';
          });
          this.hours = arrayAddHours;
          // console.log(this.hours.length);
          // Tamaño del arreglo
          const leng = this.hours.length;
          const numList = 20;
          const perPage = leng / numList;
          let index = 0;
          // tslint:disable-next-line:prefer-for-of
          for (let i = 0; i < perPage; i++) {
            const auxArray = [];
            for (let j = 0; j < numList; j++) {
              index ++;
              // console.log(i, j, index);
              const indexArray = index - 1;
              auxArray[j] = this.hours[indexArray];
            }
            this.newArray[i] = auxArray;
            this.arraySelect = this.newArray[0];
          }
          // console.log(this.newArray.length);
          let auxPag = [];
          this.newArray.forEach( function(valor, indice, array) {
            // console.log("En el índice " + indice + " hay este valor: " + valor);
            auxPag[indice] = indice + 1;
          });
          this.arraypaginador = auxPag;
            this.lentPag = this.arraypaginador.length;
            this.arrayPagMostar = this.arraypaginador.slice(this.prevPag, this.nextPag);          
            // console.log(this.arrayPagMostar);
          });
      }
    });
  }

  async getHours(token) {
    // console.log(token);
    let fecha = this.fecha_actual();
    this.hoursSrv.getHours(token, fecha)
      .then(res => {
        console.log(res);
        this.tableRegisterHour(res);
      });
  }

  tableRegisterHour(res){
    this.hoursRegister = res['registerHours'];

    // Tamaño del arreglo
    const leng = this.hoursRegister.length;
    const numList = 20;
    const perPage = leng / numList;
    let index = 0;

    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < perPage; i++) {
      const auxArray = [];
      for (let j = 0; j < numList; j++) {
        index ++;
        // console.log(i, j, index);
        const indexArray = index - 1;
        auxArray[j] = this.hoursRegister[indexArray];
      }
      this.newArrayReport[i] = auxArray;
      this.arraySelectReport = this.newArrayReport[0];
    }


     console.log(this.arraySelectReport.length);
    let auxPag = [];
    this.newArrayReport.forEach( function(valor, indice, array) {
      // console.log("En el índice " + indice + " hay este valor: " + valor);
      auxPag[indice] = indice + 1;
    });
    this.arraypaginadorRegister = auxPag;
      this.lentPag = this.arraypaginadorRegister.length;
      this.arrayPagMostarRegister = this.arraypaginadorRegister.slice(this.prevPag, this.nextPag);          
      console.log(this.arrayPagMostarRegister);
        
  }

  getDataUser() {
    this.db.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        // console.log('abrio db');
        this.db.getUrs().subscribe(usr => {
           //console.log(usr);
          this.usuario = usr[0];
          // tslint:disable-next-line:prefer-for-of
          if (this.usuario.token) {
            this.getHours(this.usuario.token);
          }
        });
      }
    });
  }

  ngOnInit() {
    this.setFilteredItems();
  }

  setFilteredItems() {
    // console.log(this.searchTerm.length );
    if (this.searchTerm.length > 2 ) {
      // console.log('entre');
      this.arraySelect = this.hours;
      this.arraySelect = this.filterItems(this.searchTerm);
    }
  }

  tabTable(i) {
    // console.log(i);
    this.arraySelect = this.newArray[i];
    this.searchTerm = '';
  }

  tabTableRegister(i) {
    // console.log(i);
    this.arraySelectReport = this.newArrayReport[i];
    this.searchTerm = '';
  }

  nextPaginator() {
    if (this.prevPag <= this.lentPag) {
      this.prevPag = this.prevPag + 4;
      this.nextPag = this.nextPag + 4;
    }
    
    // console.log('next', this.prevPag, this.nextPag);
    this.arrayPagMostar = this.arraypaginador.slice(this.prevPag, this.nextPag);
  }

  prevPaginator() {
    if (this.prevPag > 0) {
      this.prevPag = this.prevPag - 4;
      this.nextPag = this.nextPag -4;
      console.log('pre', this.prevPag, this.nextPag);
    this.arrayPagMostar = this.arraypaginador.slice(this.prevPag, this.nextPag);
    }
    
  }

  prevPaginatoRegister() {
    if (this.prevPag > 0) {
      this.prevPagRegister = this.prevPagRegister - 4;
      this.nextPagRegister = this.nextPagRegister -4;
      console.log('pre', this.prevPagRegister, this.nextPagRegister);
    this.arrayPagMostarRegister = this.arraypaginadorRegister.slice(this.prevPagRegister, this.nextPagRegister);
    }
    
  }

  nextPaginatorRegister() {
    if (this.prevPag <= this.lentPag) {
      this.prevPag = this.prevPag + 4;
      this.nextPag = this.nextPag + 4;
    }
    
    // console.log('next', this.prevPag, this.nextPag);
    this.arrayPagMostar = this.arraypaginador.slice(this.prevPag, this.nextPag);
  }
  

  changed(e) {
    //console.log(e);
    let cadena = e.detail.value;
    cadena = cadena.split('T');
    cadena = cadena[0];
    this.datos.dateSelect = cadena;
    
    cadena = this.formato(cadena);
    //console.log(cadena);
    this.arraySelectReport = this.hoursRegister;
    this.arraySelectReport = this.filterItemsHourRegister(cadena);
    this.searching = false;
  }

  changed2(cadena) {
    cadena = this.formato(cadena);
   // console.log(cadena);
   this.arraySelect = this.hours;
    this.arraySelect = this.filterItems(cadena);
    this.searching = false;
  }

  formato(texto){
    return texto.replace(/^(\d{4})-(\d{2})-(\d{2})$/g,'$3/$2/$1');
  }

  fecha_actual() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let day = '';
    let month = '';
    let yyyy = today.getFullYear();
    if (dd < 10) {
      day = '0' + dd;
    } 
    if (mm < 10) {
      month = '0' + mm;
    } 
    let todayformat = day + '/' + month + '/' + yyyy;
    return todayformat;
  }

  filterItems(searchTerm) {
    console.log(searchTerm, this.arraySelect);
    this.searching = true;
    return this.arraySelect.filter((hour) => {
      return hour.fecha_ingreso.toLowerCase().
       indexOf(searchTerm.toLowerCase()) > -1 ||
      hour.apellidos.toLowerCase().
          indexOf(searchTerm.toLowerCase()) > -1  ||
          hour.nombres.toLowerCase().
              indexOf(searchTerm.toLowerCase()) > -1;
     });
    }
    
    filterItemsHourRegister(searchTerm) {
      console.log(searchTerm, this.arraySelectReport);
      this.searching = true;
      return this.arraySelectReport.filter((hour) => {
        return hour.fecha.toLowerCase().
         indexOf(searchTerm.toLowerCase()) > -1;
       });
      }



  async presentAlertPrompt(hour) {
    const alert = await this.alertCtrl.create({
      header: 'Novedades',
      inputs: [
        {
          label: 'Nombre',
          name: 'nombres',
          value: hour.nombres + ' ' + hour.apellidos,
          type: 'text',
          placeholder: 'Placeholder 1',
          disabled : true
        },
        {
          label: 'Hora de entrada',
          name: 'Hora_entrada',
          value: hour.hora_entrada,
          type: 'text',
          placeholder: '8:00 am'
        },
        {
          label: 'Hora de salida',
          name: 'Hora_salida',
          value: hour.hora_salida,
          type: 'url',
          placeholder: 'Favorite site ever'
        },
        {
          label: 'Alimentación',
          name: 'Alimentación',
          type: 'text',
          id: 'name2-id',
          value: hour.alimentacion,
          placeholder: '0'
        },
        {
          label: 'Transporte',
          name: 'Transporte',
          value: hour.transporte,
          type: 'text',
          placeholder: '0'
        },
        {
          label: 'Observación',
          name: 'observacion',
          value: hour.observacion,
          type: 'text',
          placeholder: 'Ninguna'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (e) => {
            this.saveHourP(hour, e);
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  saveHourP(hour, e){
    // console.log(e);
    hour['hora_entrada']= e.hora_entrada;
    hour['hora_salida']= e.hora_salida;
    hour['alimentacion']= e.alimentacion;
    hour['transporte']= e.transporte;
    hour['observacion']= e.observacion;
  }

  registerHour() {
    // guardamos la info
    let dateNow = this.fecha_actual();
    //console.log(dateNow);

    //Consultamos la fecha del ultimo registro
    this.hoursSrv.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.hoursSrv.getHoursTable().then(usr => {
          console.log(usr[0]);
          if(usr[0].fecha === dateNow){
            this.toast('Ya se realizo el registro de horario para esta fecha');
          }
          else {
            this.toast('Registro de horario actualizado');
            this.hoursSrv.registerHour(dateNow, this.arraySelect, this.token).then( result=>
              {this.getEmployees();}
            );
          }
        });
        this.hoursSrv.registerHour(dateNow, this.newArray, this.token).then(date=>{console.log(date);})
      }
    });
  }

  async toast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
}

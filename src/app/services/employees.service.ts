import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { URL_SERVICE } from '../config/url.service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

export interface Employees {
    id: number;
    usuario_id: number;
    nombres: string;
    apellidos: string;
    identificacion: number;
    frente_obra: string;
}


@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  url = URL_SERVICE;
  token: string = null;

  // database
  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  usuarios = new BehaviorSubject([]);
  constructor(
    private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient,
    private httpWeb: Http) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'developers.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
      });
    });
  }

  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {
          this.loadAllUsuarios();
          this.dbReady.next(true);
        })
        .catch(e => console.error(e));
    });
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }
  getUrs(): Observable<Employees[]> {
    return this.usuarios.asObservable();
  }

  loadUsuarios(id) {
    return this.database.executeSql('SELECT * FROM employees WHERE usuario_id=?', [id]).then(data => {
      // console.log('loadUsuarios', data.rows.item(0));
      const usuarios: Employees[] = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
            usuarios.push({
            id: data.rows.item(i).id,
            usuario_id: data.rows.item(i).usuario_id,
            nombres: data.rows.item(i).nombres,
            apellidos: data.rows.item(i).apellidos,
            identificacion: data.rows.item(i).identificacion,
            frente_obra: data.rows.item(i).frente_obra
           });
        }
      }
      this.usuarios.next(usuarios);
    });
  }

  loadAllUsuarios() {
    return this.database.executeSql('SELECT * FROM employees', []).then(data => {
      console.log('loadUsuarios', data.rows.item(0));
      const empl: Employees[] = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
            empl.push({
            id: data.rows.item(i).id,
            usuario_id: data.rows.item(i).usuario_id,
            nombres: data.rows.item(i).nombres,
            apellidos: data.rows.item(i).apellidos,
            identificacion: data.rows.item(i).identificacion,
            frente_obra: data.rows.item(i).frente_obra
           });
        }
      }
      console.log(empl);
      this.usuarios.next(empl);
      return empl;
    });
  }

  addUsuarios(user) {
    this.deleteUsuario(user.usuario_id);
    // console.log('usuario recibido', user);
    const data = [user.usuario_id, user.nombres, user.apellidos, user.identificacion, user.frente_obra];
    // console.log(data, data);
    // tslint:disable-next-line:no-shadowed-variable
    return this.database.executeSql('INSERT INTO employees (usuario_id, nombres, apellidos, identificacion, frente_obra )' +
    ' VALUES (?, ?, ?, ?, ?)',
     data).then(result => {
      // console.log(result);
      this.loadUsuarios(user.usuario_id);
    },
    (error: any) => {
        // console.log(error);
     });
  }

  getUsuario(id): Promise<Employees> {
    return this.database.executeSql('SELECT * FROM employees WHERE id = ?', [id]).then(data => {

      return {
          id: data.rows.item(0).id,
          usuario_id: data.rows.item(0).usuario_id,
          nombres: data.rows.item(0).nombres,
          apellidos: data.rows.item(0).apellidos,
          identificacion: data.rows.item(0).identificacion,
          frente_obra: data.rows.item(0).frente_obra
      };
    });
  }

  deleteUsuario(id) {
    return this.database.executeSql('DELETE FROM employees WHERE usuario_id=?', [id]).then(data => {
      this.loadUsuarios(id);
    });
  }

  tokenDate(token, usuario_id, frenteobra_id) {
    let data = [token, usuario_id, frenteobra_id]
    this.database.executeSql('DELETE FROM tokenUser', []).then(data => {
      return this.database.executeSql('INSERT INTO tokenUser(token, usuario_id, frenteobra_id) VALUES(?)', data);
      
    });
  }

  getToken() {
    return new Promise( resolve => {
      this.database.executeSql('SELECT * FROM token', [])
          .then(response => {
            // console.log(response);
            resolve(response);
          })
          .catch(error => Promise.reject(error));
    });
    
  }

  updateUsuario(user: Employees) {
    // tslint:disable-next-line:prefer-const
    let data = [
      user.usuario_id,
      user.nombres,
      user.apellidos,
      user.identificacion,
      user.frente_obra
    ];
    return this.database.executeSql('UPDATE employees SET usuario_id = ?, nombres = ?, apellidos = ?, identificacion = ?, frente_obra = ?' +
    'cargo = ?, token = ?, logeado = ?' +
    // tslint:disable-next-line:no-shadowed-variable
    'WHERE id = ${user.id}', data).then(data => {
      //console.log(data);
      this.loadUsuarios(user.usuario_id);
    });
  }

  // __________________________________________http _________________________________________________ //

  selectHours( token: string, fecha: string ) {

    const data = new URLSearchParams();
    data.append('token', token);
    data.append('fecha', fecha);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post(this.url + 'consultar_horario', data, { headers } )
        .subscribe( async resp => {
          console.log(resp);
          // tslint:disable-next-line:no-string-literal
          const resJson = JSON.parse(resp['_body']);
          // tslint:disable-next-line:align
          if ( resJson.error === false) {
              // Guardamos en la base en sqlite los datos del usuario
              resolve(true);
              const userData = resJson.usuario;
              return userData;
          } else {
              resolve(false);
              return false;
          }
        },
        (error: any) => {
             // console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }

  // Editar Información de perfil
  editProfile( dataUser, photo ) {

    const data = new URLSearchParams();
    data.append('token', dataUser.token);
    data.append('photo_new', photo);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post('https://www.ardeq.com/api-ardeq/uploadPhoto.php', data, { headers } )
        .subscribe( async resp => {
          // console.log(resp);
          // tslint:disable-next-line:no-string-literal
          const resJson = JSON.parse(resp['_body']);
          if ( resJson.error === false) {
              resolve(true);
          } else {
              resolve(false);
              return false;
          }
        },
        (error: any) => {
             // console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }



}

import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { URL_SERVICE } from '../config/url.service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

export interface Hours {
    horario_id: number;
    usuario_id: number,  
    nombres: string;
    apellidos: string;
    identificacion: string; 
    frente_obra: string;   
    fecha: string;   
    inicio: string;  
    fin: string;   
    horas: string;  
    observacion: string;
    num_alimentos: string;  
    transporte: string;   
    empresa: string;   
    novedad: string; 
}

export interface registerHour {
    id : number, 
    fecha: string, 
    creador: string,
    estado: string
}

@Injectable({
  providedIn: 'root'
})
export class HoursService {

   // login
   url = URL_SERVICE;
   token: string = null;
   // database
   private database: SQLiteObject;
   private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
   hours = new BehaviorSubject([]);
   registerHoursTable : any = [];
   constructor(
     private plt: Platform,
     private sqlitePorter: SQLitePorter,
     private sqlite: SQLite,
     private http: HttpClient,
     private httpWeb: Http) {
     this.plt.ready().then(() => {
       this.sqlite.create({
         name: 'developers.db',
         location: 'default'
       })
       .then((db: SQLiteObject) => {
           this.database = db;
           this.seedDatabase();
       });
     });
   }

   seedDatabase() {
     this.http.get('assets/seed.sql', { responseType: 'text'})
     .subscribe(sql => {
       this.sqlitePorter.importSqlToDb(this.database, sql)
         .then(_ => {
           this.loadHours();
           this.dbReady.next(true);
         })
         .catch(e => console.error(e));
     });
   }

   getDatabaseState() {
     return this.dbReady.asObservable();
   }
   getHour(): Observable<Hours[]> {
     return this.hours.asObservable();
   }

   loadHours() {
     return this.database.executeSql('SELECT * FROM hours', []).then(data => {
       console.log('loadHours', data);
       const hours: Hours[] = [];
       if (data.rows.length > 0) {
         for (let i = 0; i < data.rows.length; i++) {
          hours.push({
             horario_id: data.rows.item(i).horario_id,
              usuario_id: data.rows.item(i).usuario_id, 
              nombres: data.rows.item(i).nombres,
              apellidos: data.rows.item(i).apellidos,
              identificacion: data.rows.item(i).identificacion, 
              frente_obra: data.rows.item(i).frente_obra, 
              fecha: data.rows.item(i).fecha,   
              inicio: data.rows.item(i).inicio, 
              fin: data.rows.item(i).fin,   
              horas: data.rows.item(i).horas,  
              observacion: data.rows.item(i).observacion,
              num_alimentos: data.rows.item(i).num_alimentos,  
              transporte: data.rows.item(i).transporte,
              empresa: data.rows.item(i).empresa,  
              novedad: data.rows.item(i).novedad
            });
         }
       }
       this.hours.next(hours);
     });
   }

   addHours(hours, sincronizado) {
     console.log('usuario recibido', hours);
     for (let i = 0; i < hours.length; i++) {
        const data = [hours[i].horario_id, hours[i].usuario_id, hours[i].nombres, hours[i].apellidos, hours[i].fecha, hours[i].frente_id, hours[i].editor, hours[i].edicion, 
        hours[i].comentarios, hours[i].inicio, hours[i].fin, hours[i].horas, hours[i].obs, hours[i].num_alimentos, hours[i].transporte, 
        hours[i].Observacion, hours[i].estado_id, hours[i].empresa_id, hours[i].tot, hours[i].ord, hours[i].ext, hours[i].dom, hours[i].sincronizado, hours[i].
        novedad ];
       // tslint:disable-next-line:no-shadowed-variable
       return this.database.executeSql('horario_id, usuario_id, nombres, apellidos, fecha, frente_id, editor, edicion, comentarios, inicio, fin, horas, obs, '+
       'num_alimentos, transporte, Observacion, estado_id, empresa_id, tot, ord, ext, dom, sincronizado, novedad' +
       ') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', data).then(result => {
         console.log(result);
         this.loadHours();
       },
       (error: any) => {
            console.log(error);
        });
     }
    
   }

   getHoursTable() {
    let query = 'SELECT * FROM hours';
    return this.database.executeSql(query, []).then(data => {
      let register = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          register.push({ 
            horario_id: data.rows.item(i).horario_id,
            usuario_id: data.rows.item(i).usuario_id,
            nombres: data.rows.item(i).nombres,
            apellidos: data.rows.item(i).apellidos,
            identificacion: data.rows.item(i).identificacion,
            frente_obra: data.rows.item(i).frente_obra,
            fecha: data.rows.item(i).fecha,
            inicio: data.rows.item(i).inicio,
            fin: data.rows.item(i).fin,
            horas: data.rows.item(i).horas,
            observacion: data.rows.item(i).inicio,
            num_alimentos: data.rows.item(i).num_alimentos,
            transporte: data.rows.item(i).transporte,
            empresa: data.rows.item(i).empresa,
            novedad: data.rows.item(i).num_alimentos
           });
        }
      }
      console.log(register);
      return register;
    });
   }

   getTableregisterHour(fecha): Promise<registerHour> {
    return this.database.executeSql('SELECT * FROM registerHour where fecha = ?', [fecha]).then(data => {

      return {
        id: data.rows.item(0).id,
        fecha: data.rows.item(0).fecha,
        creador: data.rows.item(0).creador,
        estado: data.rows.item(0).estado
      };
    });
  }

  getTableregisterHourAll() {
    let query = 'SELECT * FROM registerHour';
    return this.database.executeSql(query, []).then(data => {
      let register = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          register.push({ 
            id: data.rows.item(0).id,
            fecha: data.rows.item(0).fecha,
            creador: data.rows.item(0).creador,
            estado: data.rows.item(0).estado
           });
        }
      }
      console.log(register);
      return register;
    });
  }




  addRegisterHour(fecha, creador, estado) {
    let data = [fecha, creador, estado ];
    return this.database.executeSql('INSERT INTO registerHour (fecha, creador, estado) VALUES (?, ?, ?)', data).then(data => {

    });
  }

  deleteRegisterHours() {
    return this.database.executeSql('DELETE FROM registerHour', []).then(data => {
      this.loadHours();
    });
  }

  deleteHours() {
     return this.database.executeSql('DELETE FROM hours', []).then(data => {
       this.loadHours();
     });
  }

   createTable(token) {
    this.database.executeSql('DELETE FROM hours', []).then(data => {
      this.loadHours();
    });
    const sql = 'INSERT INTO tasks(title, completed) VALUES(?,?)';
    return this.database.executeSql(sql, [token]);
  }

   updateHours(user: Hours) {
     // tslint:disable-next-line:prefer-const
     let data = [
       user.usuario_id,
       user.nombres,
       user.apellidos,
       user.identificacion,
       user.frente_obra,
       user.fecha,
       user.inicio,
       user.fin,
       user.horas,
       user.observacion,
       user.num_alimentos,
       user.transporte,
       user.empresa,
       user.novedad
     ];
     return this.database.executeSql('UPDATE dataUser SET  hours.fecha = ?, hours.frente_id = ?, hours.editor = ?,  hours.edicion = ?, hours.comentarios = ?, hours.inicio = ?, hours.fin = ?, hours.horas = ?, hours.obs = ?, hours.num_alimentos = ?, hours.transporte = ?, hours.telefono = ?, hours.Observacion = ?, hours.estado_id = ?, hours.empresa_id = ?, hours.tot = ?, hours.ord = ?, hours.ext = ?, hours.dom = ?, hours.sincronizado = ?, hours.novedad = ? ]' +
     // tslint:disable-next-line:no-shadowed-variable
     'WHERE id = ${user.id}', data).then(data => {
       console.log(data);
       this.loadHours();
     });
   }

   getFechaNow(){
      // guardamos la info
    const date = new Date();

    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    let dateNow= '';

    if(month < 10){
      // console.log(`${day}-0${month}-${year}`)
      dateNow = day + '/0' + month + '/' + year;
    }else{
      // console.log(`${day}/${month}/${year}`)
      dateNow = day + '/' + month + '/' + year;
    }
    return (dateNow);

   }

   // __________________________________________login _________________________________________________ //

  getHours( token, fecha ) {
    console.log(token, fecha);
    const data = new URLSearchParams();
    data.append('token', token);
    data.append('fecha', fecha);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post(this.url + 'consultar_horario', data, { headers } )
        .subscribe( async resp => {
          console.log(resp);
          // tslint:disable-next-line:no-string-literal
          const resJson = JSON.parse(resp['_body']);
          // tslint:disable-next-line:align
          if ( resJson.ok === true) {
              // Recorremos el arreglo para identificar si la fecha seleciononada esta registrada
              resJson.forEach(function callback(posicion, index, array)  {
                if (posicion['fecha'] === fecha) {
                  //llenamos la tabla hour()
                  this.addHours(posicion, 'Sincronizado').then(
                    hours => {
                      console.log(hours);
                      //llenamos la tabla hour()
                      // si la fecha no esta en la tabla hourRegister

                      this.getTableregisterHour(fecha).then(
                        datos => {
                          console.log(datos);
                        }
                      );
                      
                      this.addRegisterHour(fecha, 'Admin', 'Sincronizado').then(
                        registerHours => {
                          let regiaterHour = this.getTableregisterHourAll();
                          let allHours = this.loadHours();
                          let arrayResult = {
                              'registerHours' : regiaterHour,
                              'hours' : allHours
                          }
                          console.log('dos arreglos1', arrayResult);
                          resolve(arrayResult);
                          return arrayResult
                        });
                    });                  
                } else{
                  this.addRegisterHour(fecha, 'Admin', 'En proceso')
                      .then( res => {
                        let regiaterHour = this.getTableregisterHourAll();
                          let allHours = this.getHour();
                          let arrayResult = {
                              'registerHours' : regiaterHour,
                              'hours' : allHours
                          }
                          console.log('dos arreglos2', arrayResult);
                          resolve(arrayResult);
                          return arrayResult
                      });
                }
                resolve(true);
                return resJson;
              });
              
          } else {
            this.getTableregisterHour(fecha).then(
              datos => {
                console.log(datos);
              });

            this.addRegisterHour(fecha, 'Admin', 'En proceso')
            .then( res => {
              this.getTableregisterHourAll().then(data=>{
                console.log('data1', data)
                let regiaterHour = data;
                this.getHoursTable().then(data=>{
                  console.log('data2', data);
                  let allHours = data;
                  let arrayResult = {
                    'registerHours' : regiaterHour,
                    'hours' : allHours
                  }
                  console.log('dos arreglos3', arrayResult);
                  resolve(arrayResult);
                  return arrayResult
                });
              });
            });
          }
        },
        (error: any) => {
             // console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }


  registerHour(date, arrayHorario, token){
    const data = new URLSearchParams();
    data.append('token', date);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post(this.url + 'registrar_horario', data, { headers } )
        .subscribe( async resp => {
          console.log(resp);
          // tslint:disable-next-line:no-string-literal
          const resJson = JSON.parse(resp['_body']);
          let sincronizado = 'No';
          // tslint:disable-next-line:only-arrow-functions
          // tslint:disable-next-line:align
          if ( resJson.error === false) {
              // Guardamos en la base en sqlite los datos del usuario
              resolve(true);
              const hoursRegister = resJson.usuario;
              sincronizado='Si';
              this.addHours(arrayHorario, sincronizado);
              return hoursRegister;
          } else {
              resolve(false);
            this.getHoursTable().then(result => 
              {
                console.log(result);
                this.addHours(arrayHorario, sincronizado);
                return result;
              } 
            );
          }
        },
        (error: any) => {
             console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }

}

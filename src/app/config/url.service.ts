// tslint:disable-next-line:eofline
export const URL_SERVICE = 'https://www.grupoc2sas.com/sistema/api/';

// Mensajes de la aplicación

export const UpdateSuccess = 'Información actualizada correctamente';
export const UpdateError = 'Ha ocurrido un error por favor uintente mas tarde';
export const ErrorConexión = 'Verifique su conexión de internet y vuelva a intentarlo';


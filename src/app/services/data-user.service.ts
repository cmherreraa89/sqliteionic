import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { URL_SERVICE } from '../config/url.service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

export interface User {
    id: number;
    usuario_id: number;
    nombres: string;
    apellidos: string;
    fotografia: string;
    tipo_identificacion_id: string;
    identificacion: number;
    exp_ciudad_id: string;
    fecha_expedicion: string;
    nacimiento_ciudad_id: string;
    email: string;
    direccion: string;
    barrio: string;
    telefono: number;
    celular: number;
    ciudad_id: number;
    cumpleanos: string;
    genero: string;
    nomina: string;
    eps_id: number;
    arl_id: number;
    afp_id: number;
    ccf_id: number;
    arl_tarifa: number;
    sueldo_real: number;
    bonificacion: number;
    tipo_bonificacion: number;
    tipo_cuenta: string;
    banco_id: number;
    cuenta: string;
    fecha_ingreso: string;
    fecha_retiro: string;
    empresa_id: number;
    perfil_id: number;
    frenteobra_id: number;
    cargo: string;
    token: string;
    logeado: string;
}

@Injectable({
  providedIn: 'root'
})
export class DataUserService {
  // login
  url = URL_SERVICE;
  token: string = null;

  // database
  private database: SQLiteObject;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  usuarios = new BehaviorSubject([]);
  constructor(
    private plt: Platform,
    private sqlitePorter: SQLitePorter,
    private sqlite: SQLite,
    private http: HttpClient,
    private httpWeb: Http) {
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'developers.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
      });
    });
  }

  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {
          this.loadAllUsuarios();
          this.dbReady.next(true);
        })
        .catch(e => console.error(e));
    });
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }
  getUrs(): Observable<User[]> {
    return this.usuarios.asObservable();
  }

  loadUsuarios(id) {
    return this.database.executeSql('SELECT * FROM dataUser WHERE usuario_id=?', [id]).then(data => {
      // console.log('loadUsuarios', data.rows.item(0));
      const usuarios: User[] = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
            usuarios.push({
            id: data.rows.item(i).id,
            usuario_id: data.rows.item(i).usuario_id,
            nombres: data.rows.item(i).nombres,
            apellidos: data.rows.item(i).apellidos,
            fotografia: data.rows.item(i).fotografia,
            tipo_identificacion_id: data.rows.item(i).tipo_identificacion_id,
            identificacion: data.rows.item(i).identificacion,
            exp_ciudad_id: data.rows.item(i).exp_ciudad_id,
            fecha_expedicion: data.rows.item(i).fecha_expedicion,
            nacimiento_ciudad_id: data.rows.item(i).nacimiento_ciudad_id,
            email: data.rows.item(i).email,
            direccion: data.rows.item(i).direccion,
            barrio: data.rows.item(i).barrio,
            telefono: data.rows.item(i).telefono,
            celular: data.rows.item(i).celular,
            ciudad_id: data.rows.item(i).ciudad_id,
            cumpleanos: data.rows.item(i).cumpleanos,
            genero: data.rows.item(i).genero,
            nomina: data.rows.item(i).nomina,
            eps_id: data.rows.item(i).eps_id,
            arl_id: data.rows.item(i).arl_id,
            afp_id: data.rows.item(i).afp_id,
            ccf_id: data.rows.item(i).ccf_id,
            arl_tarifa: data.rows.item(i).arl_tarifa,
            sueldo_real: data.rows.item(i).sueldo_real,
            bonificacion: data.rows.item(i).bonificacion,
            tipo_bonificacion: data.rows.item(i).tipo_bonificacion,
            tipo_cuenta: data.rows.item(i).tipo_cuenta,
            banco_id: data.rows.item(i).banco_id,
            cuenta: data.rows.item(i).cuenta,
            fecha_ingreso: data.rows.item(i).fecha_ingreso,
            fecha_retiro: data.rows.item(i).fecha_retiro,
            empresa_id: data.rows.item(i).empresa_id,
            perfil_id: data.rows.item(i).perfil_id,
            frenteobra_id: data.rows.item(i).frenteobra_id,
            cargo: data.rows.item(i).cargo,
            token: data.rows.item(i).token,
            logeado: data.rows.item(i).logeado
           });
        }
      }
      this.usuarios.next(usuarios);
    });
  }

  loadAllUsuarios() {
    return this.database.executeSql('SELECT * FROM dataUser', []).then(data => {
      // console.log('loadUsuarios', data.rows.item(0));
      const usuarios: User[] = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
            usuarios.push({
            id: data.rows.item(i).id,
            usuario_id: data.rows.item(i).usuario_id,
            nombres: data.rows.item(i).nombres,
            apellidos: data.rows.item(i).apellidos,
            fotografia: data.rows.item(i).fotografia,
            tipo_identificacion_id: data.rows.item(i).tipo_identificacion_id,
            identificacion: data.rows.item(i).identificacion,
            exp_ciudad_id: data.rows.item(i).exp_ciudad_id,
            fecha_expedicion: data.rows.item(i).fecha_expedicion,
            nacimiento_ciudad_id: data.rows.item(i).nacimiento_ciudad_id,
            email: data.rows.item(i).email,
            direccion: data.rows.item(i).direccion,
            barrio: data.rows.item(i).barrio,
            telefono: data.rows.item(i).telefono,
            celular: data.rows.item(i).celular,
            ciudad_id: data.rows.item(i).ciudad_id,
            cumpleanos: data.rows.item(i).cumpleanos,
            genero: data.rows.item(i).genero,
            nomina: data.rows.item(i).nomina,
            eps_id: data.rows.item(i).eps_id,
            arl_id: data.rows.item(i).arl_id,
            afp_id: data.rows.item(i).afp_id,
            ccf_id: data.rows.item(i).ccf_id,
            arl_tarifa: data.rows.item(i).arl_tarifa,
            sueldo_real: data.rows.item(i).sueldo_real,
            bonificacion: data.rows.item(i).bonificacion,
            tipo_bonificacion: data.rows.item(i).tipo_bonificacion,
            tipo_cuenta: data.rows.item(i).tipo_cuenta,
            banco_id: data.rows.item(i).banco_id,
            cuenta: data.rows.item(i).cuenta,
            fecha_ingreso: data.rows.item(i).fecha_ingreso,
            fecha_retiro: data.rows.item(i).fecha_retiro,
            empresa_id: data.rows.item(i).empresa_id,
            perfil_id: data.rows.item(i).perfil_id,
            frenteobra_id: data.rows.item(i).frenteobra_id,
            cargo: data.rows.item(i).cargo,
            token: data.rows.item(i).token,
            logeado: data.rows.item(i).logeado
           });
        }
      }
      this.usuarios.next(usuarios);
    });
  }

  addUsuarios(user) {
    this.deleteUsuario(user.usuario_id);
    // console.log('usuario recibido', user);
    const data = [user.usuario_id, user.nombres, user.apellidos, user.fotografia,
    user.tipo_identificacion_id, user.identificacion, user.exp_ciudad_id, user.fecha_expedicion, user.nacimiento_ciudad_id,
    user.email, user.direccion, user.barrio, user.telefono, user.celular, user.ciudad_id, user.cumpleanos, user.genero,
    user.nomina, user.eps_id, user.arl_id, user.afp_id, user.ccf_id, user.arl_tarifa, user.sueldo_real, user.bonificacion,
    user.tipo_bonificacion, user.tipo_cuenta, user.banco_id, user.cuenta, user.fecha_ingreso, user.fecha_retiro, user.empresa_id,
    user.perfil_id, user.frenteobra_id, user.cargo, user.token, 'si'];
    // console.log(data, data);
    // tslint:disable-next-line:no-shadowed-variable
    return this.database.executeSql('INSERT INTO dataUser (usuario_id, nombres, apellidos, fotografia, ' +
    'tipo_identificacion_id, identificacion, exp_ciudad_id, fecha_expedicion, nacimiento_ciudad_id,' +
    'email, direccion, barrio, telefono, celular, ciudad_id, cumpleanos, genero, nomina, eps_id, arl_id, ' +
    'afp_id, ccf_id, arl_tarifa, sueldo_real, bonificacion, tipo_bonificacion, tipo_cuenta, banco_id, ' +
    'cuenta, fecha_ingreso, fecha_retiro, empresa_id, perfil_id, frenteobra_id, cargo, token, logeado )' +
    ' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )',
     data).then(result => {
      // console.log(result);
      this.loadUsuarios(user.usuario_id);
    },
    (error: any) => {
        // console.log(error);
     });
  }

  getUsuario(id): Promise<User> {
    return this.database.executeSql('SELECT * FROM dataUser WHERE id = ?', [id]).then(data => {

      return {
        id: data.rows.item(0).id,
            usuario_id: data.rows.item(0).usuario_id,
            nombres: data.rows.item(0).nombres,
            apellidos: data.rows.item(0).apellidos,
            fotografia: data.rows.item(0).fotografia,
            tipo_identificacion_id: data.rows.item(0).tipo_identificacion_id,
            identificacion: data.rows.item(0).identificacion,
            exp_ciudad_id: data.rows.item(0).exp_ciudad_id,
            fecha_expedicion: data.rows.item(0).fecha_expedicion,
            nacimiento_ciudad_id: data.rows.item(0).nacimiento_ciudad_id,
            email: data.rows.item(0).email,
            direccion: data.rows.item(0).direccion,
            barrio: data.rows.item(0).barrio,
            telefono: data.rows.item(0).telefono,
            celular: data.rows.item(0).celular,
            ciudad_id: data.rows.item(0).ciudad_id,
            cumpleanos: data.rows.item(0).cumpleanos,
            genero: data.rows.item(0).genero,
            nomina: data.rows.item(0).nomina,
            eps_id: data.rows.item(0).eps_id,
            arl_id: data.rows.item(0).arl_id,
            afp_id: data.rows.item(0).afp_id,
            ccf_id: data.rows.item(0).ccf_id,
            arl_tarifa: data.rows.item(0).arl_tarifa,
            sueldo_real: data.rows.item(0).sueldo_real,
            bonificacion: data.rows.item(0).bonificacion,
            tipo_bonificacion: data.rows.item(0).tipo_bonificacion,
            tipo_cuenta: data.rows.item(0).tipo_cuenta,
            banco_id: data.rows.item(0).banco_id,
            cuenta: data.rows.item(0).cuenta,
            fecha_ingreso: data.rows.item(0).fecha_ingreso,
            fecha_retiro: data.rows.item(0).fecha_retiro,
            empresa_id: data.rows.item(0).empresa_id,
            perfil_id: data.rows.item(0).perfil_id,
            frenteobra_id: data.rows.item(0).frenteobra_id,
            cargo: data.rows.item(0).cargo,
            token: data.rows.item(0).token,
            logeado: data.rows.item(0).logeado
      };
    });
  }

  deleteUsuario(id) {
    return this.database.executeSql('DELETE FROM dataUser WHERE usuario_id=?', [id]).then(data => {
      this.loadUsuarios(id);
    });
  }


  updateUsuario(user: User) {
    // tslint:disable-next-line:prefer-const
    let data = [
      user.usuario_id,
      user.nombres,
      user.apellidos,
      user.fotografia,
      user.tipo_identificacion_id,
      user.identificacion,
      user.exp_ciudad_id,
      user.fecha_expedicion,
      user.nacimiento_ciudad_id,
      user.email,
      user.direccion,
      user.barrio,
      user.telefono,
      user.celular,
      user.ciudad_id,
      user.cumpleanos,
      user.genero,
      user.nomina,
      user.eps_id,
      user.arl_id,
      user.afp_id,
      user.ccf_id,
      user.arl_tarifa,
      user.sueldo_real,
      user.bonificacion,
      user.tipo_bonificacion,
      user.tipo_cuenta,
      user.banco_id,
      user.cuenta,
      user.fecha_ingreso,
      user.fecha_retiro,
      user.empresa_id,
      user.perfil_id,
      user.frenteobra_id,
      user.cargo,
      user.token,
      user.logeado
    ];
    return this.database.executeSql('UPDATE dataUser SET usuario_id = ?, nombres = ?, apellidos = ?, fotografia = ?, ' +
    'tipo_identificacion_id = ?, identificacion = ?, exp_ciudad_id = ?, fecha_expedicion = ?, nacimiento_ciudad_id = ?,' +
    'email = ?, direccion = ?, barriotelefono = ?, celular = ?, ciudad_id = ?, cumpleanos = ?, genero = ?, nomina = ?, ' +
    'eps_id = ?, arl_id = ?, afp_id = ?, ccf_id = ?, arl_tarifa = ?, sueldo_real = ?, bonificacion = ?, tipo_bonificacion = ?, ' +
    'tipo_cuenta = ?, banco_id = ?, cuenta = ?, fecha_ingreso = ?, fecha_retiro = ?, empresa_id = ?, perfil_id = ?, frenteobra_id' +
    'cargo = ?, token = ?, logeado = ?' +
    // tslint:disable-next-line:no-shadowed-variable
    'WHERE id = ${user.id}', data).then(data => {
      //console.log(data);
      this.loadUsuarios(user.usuario_id);
    });
  }

  // __________________________________________login _________________________________________________ //

  login( identificacion: string, password: string ) {

    const data = new URLSearchParams();
    data.append('identificacion', identificacion);
    data.append('password', password);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post(this.url + 'loguearse', data, { headers } )
        .subscribe( async resp => {
          // console.log(resp);
          // tslint:disable-next-line:no-string-literal
          const resJson = JSON.parse(resp['_body']);
          // tslint:disable-next-line:only-arrow-functions
          // tslint:disable-next-line:align
          if ( resJson.error === false) {
              // Guardamos en la base en sqlite los datos del usuario
              resolve(true);
              const userData = resJson.usuario;
              this.addUsuarios(userData);
              return userData;
          } else {
              resolve(false);
              return false;
          }
        },
        (error: any) => {
             // console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }

  // Editar Información de perfil
  editProfile( dataUser, photo ) {

    const data = new URLSearchParams();
    data.append('token', dataUser.token);
    data.append('photo_new', photo);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post('https://www.ardeq.com/api-ardeq/uploadPhoto.php', data, { headers } )
        .subscribe( async resp => {
          // console.log(resp);
          // tslint:disable-next-line:no-string-literal
          const resJson = JSON.parse(resp['_body']);
          if ( resJson.error === false) {
              resolve(true);
          } else {
              resolve(false);
              return false;
          }
        },
        (error: any) => {
             // console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }



}

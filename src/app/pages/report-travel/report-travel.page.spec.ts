import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTravelPage } from './report-travel.page';

describe('ReportTravelPage', () => {
  let component: ReportTravelPage;
  let fixture: ComponentFixture<ReportTravelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTravelPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTravelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

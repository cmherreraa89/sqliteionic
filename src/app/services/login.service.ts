import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { URL_SERVICE } from '../config/url.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = URL_SERVICE;


  token: string = null;

   constructor( private http: Http) { }


   login( identificacion: string, password: string ) {

     const data = new URLSearchParams();
     data.append('identificacion', identificacion);
     data.append('password', password);
     const headers = new Headers();
     headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');


     return new Promise( resolve => {
      // this.http.post(`https://www.ardeq.com/api-ardeq/public/Ingresar`, data )
       this.http.post('https://www.ardeq.com/repositories/backc2/api/consultar_horario', data, { headers } )
         .subscribe( async resp => {
           // console.log(resp);
           if ( resp ) {
             resolve(true);
           } else {
             this.token = null;
             resolve(false);
           }
           return resp;
         },
         (error: any) => {
              // console.log(error);
              resolve(false);
              return error;
          }
        );
     });
   }

 }

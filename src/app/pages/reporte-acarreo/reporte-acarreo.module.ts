import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReporteAcarreoPage } from './reporte-acarreo.page';

const routes: Routes = [
  {
    path: '',
    component: ReporteAcarreoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReporteAcarreoPage]
})
export class ReporteAcarreoPageModule {}

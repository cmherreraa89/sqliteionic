CREATE TABLE IF NOT EXISTS developer(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,skills TEXT,img TEXT);
INSERT or IGNORE INTO developer VALUES (1, 'Simon', '', 'https://pbs.twimg.com/profile_images/858987821394210817/oMccbXv6_bigger.jpg');
INSERT or IGNORE INTO developer VALUES (2, 'Max', '', 'https://pbs.twimg.com/profile_images/953978653624455170/j91_AYfd_400x400.jpg');
INSERT or IGNORE INTO developer VALUES (3, 'Ben', '', 'https://pbs.twimg.com/profile_images/1060037170688417792/vZ7iAWXV_400x400.jpg');
 
CREATE TABLE IF NOT EXISTS product(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT, creatorId INTEGER);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (1, 'Sin Almuerzo', 1);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (2, 'Con almuerzo', 1);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (3, 'Incapacidad', 2);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (4, 'otro', 2);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (5, 'otro ', 3);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (6, 'fad', 3);


CREATE TABLE IF NOT EXISTS dataUser(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    usuario_id INTEGER,
    nombres TEXT,
    apellidos TEXT,
    fotografia TEXT,
    tipo_identificacion_id TEXT,
    identificacion TEXT,
    exp_ciudad_id TEXT,
    fecha_expedicion TEXT,
    nacimiento_ciudad_id TEXT,
    email TEXT, 
    direccion TEXT,
    barrio TEXT,
    telefono TEXT,
    celular TEXT,
    ciudad_id TEXT,
    cumpleanos TEXT,
    genero TEXT,
    nomina TEXT,
    eps_id TEXT,
    arl_id TEXT,
    afp_id TEXT,
    ccf_id TEXT,
    arl_tarifa TEXT,
    sueldo_real TEXT,
    bonificacion TEXT,
    tipo_bonificacion TEXT,
    tipo_cuenta TEXT,
    banco_id TEXT,
    cuenta TEXT,
    fecha_ingreso TEXT,
    fecha_retiro TEXT,
    empresa_id TEXT,
    perfil_id TEXT,
    frenteobra_id TEXT,
    cargo TEXT,
    token TEXT,
    logeado TEXT
);

CREATE TABLE IF NOT EXISTS employees(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    usuario_id INTEGER,
    nombres TEXT,
    apellidos TEXT,
    identificacion TEXT,
    frente_obra TEXT
);

CREATE TABLE IF NOT EXISTS hours(
    horario_id INTEGER PRIMARY KEY AUTOINCREMENT,
    usuario_id INTEGER, 
    nombres  TEXT, 
    apellidos  TEXT, 
    identificacion  TEXT,   
    frente obra  INTEGER,   
    fecha TEXT,  
    inicio  TEXT,   
    fin  TEXT,   
    horas  INTEGER,  
    Observacion  TEXT,   
    num_alimentos  INTEGER,   
    transporte  INTEGER,   
    empresa  TEXT,   
    novedad TEXT
);

-- Estados: sincronizado, no sincronizado, en proceso
CREATE TABLE IF NOT EXISTS registerHour(
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    fecha INTEGER, 
    creador TEXT, 
    estado TEXT
);

CREATE TABLE IF NOT EXISTS travel(
    id INTEGER PRIMARY KEY AUTOINCREMENT, 
    fecha TEXT, 
    placa TEXT, 
    operador TEXT, 
    hora_entrada TEXT, 
    hora_salida TEXT,
    hora_inicial TEXT,
    hora_final TEXT,
    tiempo_descuento TEXT,
    total_horas TEXT,
    diesel_surtido TEXT,
    costo_diesel TEXT,
    material TEXT,
    frente_obra TEXT,
    fuente TEXT,
    destino TEXT,
    observaciones TEXT,
    aceptado_por TEXT,
    factura TEXT,
    cheque TEXT,
    valor TEXT,
    estado TEXT
);



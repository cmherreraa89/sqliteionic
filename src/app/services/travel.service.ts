import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { URL_SERVICE } from '../config/url.service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject, Observable } from 'rxjs';

export interface Travel {
    id: number;
    fecha: string;
    placa: string; 
    operador: string; 
    hora_entrada: string;
    hora_salida: string;
    hora_inicial: string;
    hora_final: string;
    tiempo_descuento: string;
    total_horas: string;
    diesel_surtido: string;
    costo_diesel: string;
    material: string;
    frente_obra: string;
    fuente: string;
    destino: string;
    observaciones: string;
    aceptado_por: string;
    factura: string;
    cheque : string;
    valor: number;
    estado: string;
}

@Injectable({
  providedIn: 'root'
})
export class TravelService {

  url = URL_SERVICE;
   token: string = null;
   // database
   private database: SQLiteObject;
   private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
   travel = new BehaviorSubject([]);

  constructor(
    private plt: Platform,
     private sqlitePorter: SQLitePorter,
     private sqlite: SQLite,
     private http: HttpClient,
     private httpWeb: Http
  ) { 
    this.plt.ready().then(() => {
      this.sqlite.create({
        name: 'developers.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.database = db;
          this.seedDatabase();
      });
    });
  }

  seedDatabase() {
    this.http.get('assets/seed.sql', { responseType: 'text'})
    .subscribe(sql => {
      this.sqlitePorter.importSqlToDb(this.database, sql)
        .then(_ => {
          this.loadTravel();
          this.dbReady.next(true);
        })
        .catch(e => console.error(e));
    });
  }

  loadTravel() {
    return this.database.executeSql('SELECT * FROM hours', []).then(data => {
      console.log('loadHours', data);
      const travels: Travel[] = [];
      if (data.rows.length > 0) {
        for (let i = 0; i < data.rows.length; i++) {
         travels.push({
            id: data.rows.item(i).id,
             fecha: data.rows.item(i).fecha, 
             placa: data.rows.item(i).placa,
             operador: data.rows.item(i).operador,
             hora_entrada: data.rows.item(i).hora_entrada, 
             hora_salida: data.rows.item(i).hora_salida, 
             hora_inicial: data.rows.item(i).hora_inicial,   
             hora_final: data.rows.item(i).hora_final, 
             tiempo_descuento: data.rows.item(i).tiempo_descuento,   
             total_horas: data.rows.item(i).total_horas,  
             diesel_surtido: data.rows.item(i).diesel_surtido,
             costo_diesel: data.rows.item(i).costo_diesel,  
             material: data.rows.item(i).material,
             frente_obra: data.rows.item(i).frente_obra,  
             fuente: data.rows.item(i).fuente,
             destino: data.rows.item(i).destino,  
             observaciones: data.rows.item(i).observaciones,
             aceptado_por: data.rows.item(i).aceptado_por,  
             factura: data.rows.item(i).factura,
             cheque: data.rows.item(i).cheque,  
             valor: data.rows.item(i).valor,
             estado: data.rows.item(i).estado
           });
        }
      }
      return (travels);

    });
  }

  addTravel(travel, sincronizado) {
    console.log('usuario recibido', travel);
    for (let i = 0; i < travel.length; i++) {
       const data = [ travel[i].fecha, travel[i].placa, travel[i].operador, travel[i].hora_entrada, travel[i].hora_salida, travel[i].hora_inicial, travel[i].hora_final, 
       travel[i].tiempo_descuento, travel[i].total_horas, travel[i].diesel_surtido, travel[i].costo_diesel, travel[i].material, travel[i].frente_obra, travel[i].fuente, 
       travel[i].destino, travel[i].observaciones, travel[i].aceptado_por, travel[i].factura, travel[i].cheque, travel[i].valor, travel[i].estado];
      // tslint:disable-next-line:no-shadowed-variable
      return this.database.executeSql(' fecha, placa, operador, hora_entrada, hora_salida, hora_inicial, hora_final, tiempo_descuento, total_horas, diesel_surtido, costo_diesel, material, '+
          'frente_obra, fuente, destino, observaciones, aceptado_por, factura, cheque, valor, estado' +
          ') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', data).then(result => {
            console.log(result);
            this.loadTravel();
          },
          (error: any) => {
              console.log(error);
       });
    }
   
  }

  deleteTravel() {
    return this.database.executeSql('DELETE FROM travel', []).then(data => {
      this.loadTravel();
    });
  }

  updateDeveloper(travel) {
    let data = [travel.estado];
    return this.database.executeSql(`UPDATE travel SET estado = ? WHERE id = ${travel.id}`, data).then(data => {
      this.loadTravel();
    })
  }

  // __________________________________________ http _________________________________________________ //
    

  getRemote( token, fecha ) {
    console.log(token, fecha);
    const data = new URLSearchParams();
    data.append('token', token);
    data.append('fecha', fecha);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post(this.url + 'consultar_viajes', data, { headers } )
        .subscribe( async resp => {
          console.log(resp);
          resolve(resp);
          return resp
        },
        (error: any) => {
             // console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }

  pendingRecord(){
    const record = [];
    this.loadTravel().then(datos =>{
      datos.forEach(function callback(posicion, index, array)  {
        if (posicion['estado'] == 'no sincronizado') {

        }
      });
    });
  }
  
  getLocal(){
    this.loadTravel().then(data=>{
      
    });
  }

  insertLocal(data, estado){
    this.addTravel(data, estado).then(data => {

    });
  }

  deleteLocal(){
    this.deleteTravel().then(data => {

    });
  }

  InsertRemote(travels, token){
    console.log(travels, token);
    const data = new URLSearchParams();

    data.append('fecha', travels.fecha);
    data.append('placa', travels.placa);
    data.append('operador', travels.operador);
    data.append('hora_entrada', travels.hora_entrada);
    data.append('hora_salida', travels.hora_salida);
    data.append('hora_inicial', travels.hora_inicial);
    data.append('hora_final', travels.hora_final);
    data.append('tiempo_descuento', travels.tiempo_descuento);
    data.append('total_horas', travels.total_horas);
    data.append('diesel_surtido', travels.diesel_surtido);
    data.append('costo_diesel', travels.costo_diesel);
    data.append('material', travels.material);
    data.append('frente_obra', travels.frente_obra);
    data.append('fuente', travels.fuente);
    data.append('destino', travels.destino);
    data.append('observaciones', travels.observaciones);
    data.append('aceptado_por', travels.aceptado_por);
    data.append('factura', travels.factura);
    data.append('cheque', travels.cheque);
    data.append('valor', travels.valor);
    data.append('estado', travels.estado);
    const headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

    return new Promise( resolve => {
      this.httpWeb.post(this.url + 'consultar_horario', data, { headers } )
        .subscribe( async resp => {
          console.log(resp);
          resolve(resp);
          return resp
        },
        (error: any) => {
             // console.log(error);
             resolve(false);
             return error;
         }
       );
    });
  }

  


}
